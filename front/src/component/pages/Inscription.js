import React, {useEffect,useState } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { registerUser } from "../util/Api";

const initialState ={
    nom: "",
    prenom: "",
    DOB:"",
    email: "",
    Password:"",
    Confirmpass:"",
};

//state Verification des normes du mot de passe
const passVerificationError ={
    islenthy:false,
    hasUper:false,
    hasLower:false,
    hasNumber:false,
    hasSpclChr:false,
    Confirmpass:false,
    
};

 const Inscription =() =>{
     const [newUser, setnewUser] = useState(initialState);
     const[passwordError,setPasswordError] = useState(passVerificationError);

     useEffect(() =>{}, [newUser]);

     const handleOnChange = (e) =>{
         const{name, value} = e.target;
         setnewUser({...newUser,[name]:value});

         console.log(newUser)
         
         //verifier le respect des règles du mot de passe
         if(name ==="Password"){
           const islenthy= value.length > 8;
           const hasUper = /[A-Z]/.test(value);
           const hasLower = /[a-z]/.test(value);
           const hasNumber = /[0-9]/.test(value);
            const hasSpclChr = /[@,#,$,%,&]/.test(value);

            setPasswordError({...passwordError,islenthy,
                hasUper,hasLower,hasNumber,hasSpclChr});
         }
         
         //Verifier si confirmpass = password
            if(name === 'Confirmpass'){
                setPasswordError({...setPasswordError,
                    Confirmpass:  newUser.Password === value});
            }

    
    
        };

        //fonction pour enregistrer les données
        const handleOnSubmit =(e) =>{
            e.preventDefault();
            console.log(newUser);
            registerUser(newUser)
            .then(res => {
              console.log(res);
              console.log(res.data);
            })
        
        }
 
        return (

            <Container>
                <Row >
                    <Col>
                    <h1 className="inscription-form"><span className="font-weight-bold ">Inscription au réseau</span></h1>
                    </Col>
                </Row>

                <Row>
                    <Col>
 <Form className="inscription-form" onSubmit={handleOnSubmit}>

    <Form.Group className="mb-3" controlId="formBasicEmail">
    <Form.Label>Nom</Form.Label>
    <Form.Control type="text" name="nom" value={newUser.nom} onChange={handleOnChange} placeholder="Nom" />
  </Form.Group>

  <Form.Group className="mb-3" controlId="formBasicEmail">
    <Form.Label>Prénom</Form.Label>
    <Form.Control type="text" name="prenom" value={newUser.prenom} onChange={handleOnChange} placeholder="prenom" />
  </Form.Group>

  <Form.Group className="mb-3" controlId="formBasicEmail">
    <Form.Label>Date de naissance</Form.Label>
    <Form.Control type="date" name="DOB"  value={newUser.DOB} onChange={handleOnChange} placeholder="Date de naissance" />
  </Form.Group>

  <Form.Group className="mb-3" controlId="formBasicEmail">
    <Form.Label>Email address</Form.Label>
    <Form.Control type="email" name="email" value={newUser.email}  onChange={handleOnChange} placeholder="Enter email" />
  </Form.Group>

  <Form.Group className="mb-3" controlId="formBasicPassword">
    <Form.Label>Password</Form.Label>
    <Form.Control type="password"  name="Password" value={newUser.Password} onChange={handleOnChange} placeholder="Password" />
  </Form.Group>

  <Form.Group className="mb-3" controlId="formBasicPassword">
    <Form.Label>Confirm Password</Form.Label>
    <Form.Control type="password"  name="Confirmpass" value={newUser.Confirmpass} onChange={handleOnChange} placeholder="Confirm Password" />
  </Form.Group>
  <Form.Text>{!passwordError.Confirmpass &&(<div className="text-danger mb-3">Mot de passe est différent</div>)}</Form.Text>
  <ul className="mb-4">
      <li className={passwordError.islenthy ? "text-success" : "text-danger"}>Min 8 charactères</li>
      <li className={passwordError.hasUper ? "text-success" : "text-danger"}>Au moins une lettre majuscule</li>
      <li className={passwordError.hasLower ? "text-success" : "text-danger"}>Au moins une lettre miniscule</li>
      <li className={passwordError.hasNumber ? "text-success" : "text-danger"}>Au moins un chiffre</li>
      <li className={passwordError.hasSpclChr ? "text-success" : "text-danger"}>Au moins un caractère special i.e @ # $ % &</li>
  </ul>
  <div className ="d-grid gap-2">
  <Button className="btn-lg btn-dark btn-block"  type="submit" disabled={Object.values(passwordError).includes(false)}>
    Inscription
  </Button>
  </div>
</Form>
 </Col>
    </Row>
     </Container>
           
            
        );
    }

    export default Inscription;
