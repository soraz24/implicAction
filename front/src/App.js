import React from 'react'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavbarLink from './component/pages/NavbarLink';
import Footer from './component/pages/Footer';


function App() {
  return (
    <div className="">
      <NavbarLink/>
      <Footer/>
    </div>
    
   
  );
}

export default App;
