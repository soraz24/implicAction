import React, { Component } from "react";
import { Carousel } from "react-bootstrap";

export default class Slideshow extends Component {
  render() {
    return (
      <div>
        <Carousel>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src="https://implicaction.eu/wp-content/uploads/2020/06/Photo-officielle-ImplicAction.jpg"
              alt="First slide"
            />
            <Carousel.Caption>
              {/* <h3>First slide label</h3> */}
              <p>Bienvenu sur le site ImplicAction</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src="https://implicaction.eu/wp-content/uploads/2020/06/Photo-officielle-ImplicAction.jpg"
              alt="Second slide"
            />

            <Carousel.Caption>
              {/* <h3>Second slide label</h3> */}
              <p>
                Notre réseau c'est vous garantir le meilleur au niveau social,
                professionnel.
              </p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src="https://implicaction.eu/wp-content/uploads/2020/06/Photo-officielle-ImplicAction.jpg"
              alt="Third slide"
            />

            <Carousel.Caption>
              {/* <h3>Third slide label</h3> */}
              <p>
                Nous vous assistons dans la recherche du travail après votre
                carrière .
              </p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </div>
    );
  }
}
