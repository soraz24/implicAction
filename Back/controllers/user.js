const User = require('../models/user');
const bcrypt = require('bcrypt');

exports.createUser = (req, res, next) => {
    bcrypt.hash(req.body.password, 10)
    .then(hash => {
        const user = new User({
            login: req.body.login,
            email: req.body.email,
            password: hash,
            nom: req.body.nom,
            prenom: req.body.prenom,
            dateCreation: new Date(),
            activationKey: true,
            dateNaiss: req.body.dateNaiss
          });
          user.save()
            .then(() => res.status(201).json({ message: 'Utilisateur enregistré !'}))
            .catch(error => res.status(400).json({ error: error }));
    })
    .catch(error => res.status(500).json({ error }));
    
};

exports.getUserByEmail = (req, res, next) => {
    User.findOne({ email: req.params.email })
      .then(user => res.status(200).json(user))
      .catch(error => res.status(404).json({ error: error }));
};

exports.updateUser = (req, res, next) => {
    User.updateOne({ email: req.params.email }, { ...req.body, email: req.params.email})
      .then(user => res.status(200).json({ message: 'Objet modifié'}))
      .catch(error => res.status(404).json({ error: error }));
};

exports.deleteUser = (req, res, next) => {
    User.deleteOne({ email: req.params.email })
      .then(() => res.status(200).json({ message: 'Objet supprimé !'}))
      .catch(error => res.status(400).json({ error: error }));
};

exports.getAllUsers = (req, res, next) => {
    User.find()
      .then(users => res.status(200).json(users))
      .catch(error => res.status(400).json({ error: error }));
}

exports.login = (req, res, next) => {

}