import React from "react";
import { Navbar, Nav, NavDropdown, Container, Button } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Home from "./Home";
import Histoire from "./Histoire";
import Entreprise from "./Entreprise";
import Login from "./Login";
import Inscription from "./Inscription";
import Membres from "./Membres";
import Reseau from "./Reseau";
import Devise from "./Devise";
import Partenaires from "./Partenaires";
import Delegues from "./Delegues";
import Signup from "./Signup";

export default function NavbarLink() {
  return (
    <Router>
      <>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          <Container>
            <Navbar.Brand as={Link} to={"/"}>
              <img
                src="/logo.jpg"
                width="150"
                height="50"
                className="d-inline-block align-top"
                alt="logo"
              />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="me-auto">
                <Nav.Link as={Link} to={"/"}>
                  Accueil
                </Nav.Link>
                <NavDropdown
                  title="à propos de nous"
                  id="collasible-nav-dropdown"
                >
                  <NavDropdown.Item as={Link} to={"/histoire"}>
                    Histoire
                  </NavDropdown.Item>
                  <NavDropdown.Item as={Link} to={"/devise"}>
                    Devise
                  </NavDropdown.Item>
                  <NavDropdown.Item as={Link} to={"/membre"}>
                    Membres du bureau
                  </NavDropdown.Item>
                  <NavDropdown.Item as={Link} to={"/reseau"}>
                    Réseau
                  </NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item as={Link} to={"/delegue"}>
                    Délégués Régionaux
                  </NavDropdown.Item>
                </NavDropdown>
                <NavDropdown
                  title="Espace entreprise"
                  id="collasible-nav-dropdown"
                >
                  <NavDropdown.Item as={Link} to={"/entreprise"}>
                    Entreprise
                  </NavDropdown.Item>
                  <NavDropdown.Item as={Link} to={"/partenaires"}>
                    Nos partenaires
                  </NavDropdown.Item>
                  <NavDropdown.Item as={Link} to={"/offres"}>
                    Offres d'emploi
                  </NavDropdown.Item>
                </NavDropdown>

                <NavDropdown
                  title="Nous rejoindre"
                  id="collasible-nav-dropdown"
                >
                  <NavDropdown.Item as={Link} to={"/adherer"}>
                    Adhérer à l'association
                  </NavDropdown.Item>
                  <NavDropdown.Item as={Link} to={"/inscription"}>
                    S'incrire au réseau
                  </NavDropdown.Item>
                </NavDropdown>
              </Nav>
              <Nav>
                <Nav.Link eventKey={2} as={Link} to={"/login"}>
                  <Button>Se connecter</Button>
                </Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </>
      <div>
        <Switch>
          <Route path="/histoire">
            <Histoire />
          </Route>
          <Route path="/entreprise">
            <Entreprise />
          </Route>
          <Route path="/inscription">
            <Inscription />
          </Route>

          <Route path="/membre">
            <Membres />
          </Route>

          <Route path="/reseau">
            <Reseau />
          </Route>

          <Route path="/devise">
            <Devise />
          </Route>

          <Route path="/partenaires">
            <Partenaires />
          </Route>

          <Route path="/delegue">
            <Delegues />
          </Route>

          <Route path="/login">
            <Login />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
      <div></div>
    </Router>
  );
}
