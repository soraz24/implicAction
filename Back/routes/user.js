const express = require('express');

const router = express.Router();

const auth = require('../middleware/auth');

const userController = require('../controllers/user');

router.post('/', userController.createUser);
  
router.get('/:email', auth,  userController.getUserByEmail);
  
router.put('/:email', auth, userController.updateUser);
  
router.delete('/:email', auth,  userController.deleteUser);

router.get('/', auth, userController.getAllUsers);


module.exports = router;