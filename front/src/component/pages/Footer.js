import React, { Component } from "react";
import './Footer.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import Actualites from "./Actualites";
import Evenement from "./Evenement";
import LegalMention from "./LegalMention";


export default class Footer extends Component{
    render(){
        return(
            <Router>
            <div>
                <div className= "page-footer bg-dark">

                    <div className="bg-success">
                        <div className="container">
                            <div className = "row py-4 d-flex align-items-center">

                                <div className="col-md-12 text-center font-weight-bold text-lowercase  text-dark social-links">
                                Suivez-nous sur &nbsp; &nbsp;
                                    <a href="https://www.facebook.com/groups/973914722626972/" target="_blank" rel="noreferrer"><i class="fab fa-facebook-f  mx-2 d-inline-block"></i></a>
                                    <a href="https://www.linkedin.com/groups/4491433/" target="_blank" rel="noreferrer"><i class="fab fa-linkedin-in mx-2 d-inline-block"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="container text-center text-md-left mt-5 text-white">
                        <div className="row">

                            <div className="col-md-3 mx-auto mb-4">
                                <h6 className ="text-uppercase font-weight-bold text-white">Evénements</h6>
                                <hr className="bg-sucess  mt-0 d-inline-block mx-auto" style={{width:'110px', height:'2px'}}/>
                                <ul className="list-unstyled">
                                    <li className="my-2 text-white "><Link to="/Evenement">Accès aux évenements</Link></li>
                                </ul>
                            </div>

                            <div className="col-md-3 mx-auto mb-4">
                            <h6 className ="text-uppercase font-weight-bold text-white">Actualités</h6>
                               <hr className="bg-sucess  mt-0  d-inline-block mx-auto" style={{width:'75px', height:'2px'}}></hr>
                                
                                <ul className="list-unstyled">
                                    <li className="my-2"><Link to="/actualites">Nos actualités</Link></li>
                                </ul>
                            </div>

                            <div className="col-md-3 mx-auto mb-4 text-white">
                            <h6 className ="text-uppercase font-weight-bold">Contact</h6>
                               <hr className="bg-sucess  mt-0 d-inline-block mx-auto" style={{width:'110px', height:'2px'}}></hr>
                                 
                                <ul className="list-unstyled">
                                    <li className="my-2"><i className="fas fa-phone"></i> +33(0)5645345</li>
                                    <li className="my-2"><i className="fas fa-envelope"></i> info@implic.eu</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
             
                <div className="footer-copyright text-center py-3 footer-bottom">
                    <p p className="text-xs-center"> 
                    &copy;{new Date().getFullYear()} Groupe4 | Tous droits réservés | <Link to ="/mentionlegal">Mention légal</Link>
                    </p>
                </div>

  
            </div>
            <div>
<Switch>
          <Route path="/Evenement">
            <Evenement/>
          </Route>
          <Route path="/actualites">
            <Actualites/>
          </Route>

          <Route path="/mentionlegal">
            <LegalMention/>
          </Route>
         
</Switch>
</div>
            </Router>
        )
    }
}