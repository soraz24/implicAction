import React, { Component } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";

export default class Signup extends Component {
  render() {
    return (
      <Container>
        <Row>
          <Col>
            <h1>Inscription au Réseau</h1>
          </Col>
        </Row>
        <hr />
        <Row>
          <Col>
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Nom et Prénom</Form.Label>
                <Form.Control type="text" placeholder="Nom et Prenom" />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Date de naissance</Form.Label>
                <Form.Control type="date" placeholder="Date de naissance" />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control type="email" placeholder="Enter email" />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Mot de Passe</Form.Label>
                <Form.Control type="password" placeholder="Password" />
              </Form.Group>

              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}
