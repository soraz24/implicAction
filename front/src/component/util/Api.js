import axios from 'axios';

const rootUrl = 'http://localhost:8000'

export const registerUser=(user) => {
    return  axios.post(`${rootUrl}/api/user`, { user })
}

export const getUserToken=(newlogin) => {
    return  axios.post(`${rootUrl}/api/login`, { email: newlogin.email, password:  newlogin.password })
}