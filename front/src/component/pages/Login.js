import React, { useEffect, useState } from "react";
import { Form, Button } from "react-bootstrap";
import { getUserToken } from "../util/Api";
import { Link } from "react-router-dom";

const initialState = {
  email: "",
  Password: "",
};

const Login = () => {
  const [newlogin, setnewlogin] = useState(initialState);

  useEffect(() => {}, [newlogin]);

  const handleOnChange = (e) => {
    const { name, value } = e.target;
    setnewlogin({ ...newlogin, [name]: value });

    console.log(newlogin);
  };

  //fonction pour enregistrer les données
  const handleOnSubmit = (e) => {
    e.preventDefault();
    console.log(newlogin);
    getUserToken(newlogin).then((res) => {
      console.log(res);
      console.log(res.data);
    });
  };

  return (
    <Form className="login-form" onSubmit={handleOnSubmit}>
      <h1>
        <span className="font-weight-bold">Authentification</span>
      </h1>
      <Form.Group className="mb-3 mt-3" controlId="formBasicEmail">
        <Form.Label> Email</Form.Label>
        <Form.Control
          type="email"
          name="email"
          placeholder="Enter email"
          value={newlogin.email}
          onChange={handleOnChange}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Mot de passe</Form.Label>
        <Form.Control
          type="password"
          name="Password"
          value={newlogin.Password}
          onChange={handleOnChange}
          placeholder="Password"
        />
      </Form.Group>
      <div className="d-grid gap-2">
        <Button className="btn-lg btn-dark btn-block " type="submit">
          Connexion
        </Button>
      </div>
      <div className="text-center mt-3">
        <a href="/inscription">Inscription</a>
        <span className="p-2">|</span>
        <Link to={"/inscription"}>Mot de passe oublié?</Link>
      </div>
    </Form>
  );
};

export default Login;
